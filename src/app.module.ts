import { Module } from '@nestjs/common';
import { DownloadModule } from './downloads/downloads.module';

@Module({
  imports: [
    DownloadModule
  ],
})
export class AppModule { }

import * as mongoose from 'mongoose';

export const DownloadSchema = new mongoose.Schema({
  url: String,
  status: String
});
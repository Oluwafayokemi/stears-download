import { Document } from 'mongoose';

export interface Download extends Document {
  readonly url: string;
}
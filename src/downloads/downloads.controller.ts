import { Controller, Get, Post, Body } from '@nestjs/common';
import { DownloadsService } from './downloads.service';
import { CreateDownloadDto } from './dto/create-download.dto';
import { Download } from './interfaces/download.interface'

@Controller()
export class DownloadController {
  constructor(private readonly downloadService: DownloadsService) { }

  @Post('downloads')
  async create(@Body() body: CreateDownloadDto) {
    try {
      return this.downloadService.create(body);
    }
    catch (err) {
      throw new Error(err)
    }
  }

  @Get('downloads')
  async get(): Promise<Download[]> {
    return this.downloadService.findAll()
  }
}

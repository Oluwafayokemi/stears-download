import { Connection } from 'mongoose';
import { DownloadSchema } from './schemas/download.schema';
import { DOWNLOAD_MODEL, DATABASE_CONNECTION } from 'src/downloads/constants';

export const downloadsProviders = [
  {
    provide: DOWNLOAD_MODEL,
    useFactory: (connection: Connection) => connection.model('Download', DownloadSchema),
    inject: [DATABASE_CONNECTION],
  },
];
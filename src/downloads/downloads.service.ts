import { Model } from 'mongoose';
import { Injectable, Inject } from '@nestjs/common';
import { Download } from './interfaces/download.interface';
import { CreateDownloadDto } from './dto/create-download.dto';
import { DOWNLOAD_MODEL } from 'src/downloads/constants';
import { DownloadQueues } from './queues';

@Injectable()
export class DownloadsService {
  constructor(
    @Inject(DOWNLOAD_MODEL)
    private downloadModel: Model<Download>,
    private downloadQueues: DownloadQueues) { }

  async create(createDownloadDto: CreateDownloadDto): Promise<Download> {
    try {
      const createdDownload = new this.downloadModel({ ...createDownloadDto, status: 'pending' });
      const newDownload = await createdDownload.save();
      this.downloadQueues.addToQueue({ id: newDownload._id, url: newDownload.url, retries: 0 })
      return newDownload
    }
    catch (err) {
      throw new Error(err)
    }
  }

  async findAll(): Promise<Download[]> {
    return this.downloadModel.find().exec();
  }
  async findOneAndUpdate(filterBy, update): Promise<Download> {
    const newUpdate = await this.downloadModel.findOneAndUpdate(filterBy, update);
    return newUpdate
  }
}
import { Test, TestingModule } from '@nestjs/testing';
import { DownloadController } from './downloads.controller';
import { DownloadsService } from './downloads.service';

const body = {
  url: ''
}

describe('DownloadController', () => {
  let downloadController: DownloadController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [DownloadController],
      providers: [DownloadsService],
    }).compile();

    downloadController = app.get<DownloadController>(DownloadController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(downloadController.create(body)).toBe(body);
    });
  });
  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(downloadController.get()).toBe(body);
    });
  });
});

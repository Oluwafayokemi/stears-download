import { Module } from '@nestjs/common';
import { DownloadController } from './downloads.controller';
import { DownloadsService } from './downloads.service';
import { DatabaseModule } from '../database/database.module'
import { downloadsProviders } from './downloads.provider';
import { BullModule } from '@nestjs/bull';
import { DownloadQueues } from './queues'
import { DownloadConsumer } from './queues/queue.consumer'

@Module({
  imports: [
    DatabaseModule,
    BullModule.registerQueue({
      name: 'file',
      redis: {
        host: 'localhost',
        port: 6379,
      },
    }),
  ],
  controllers: [DownloadController],
  providers: [DownloadsService, ...downloadsProviders, DownloadQueues, DownloadConsumer],
})
export class DownloadModule { }


import { ObjectId } from 'mongodb';
import { Processor, Process } from '@nestjs/bull';
import { Job } from 'bull';
import { DownloadQueues } from './';
import { DownloadsService } from '../downloads.service';
import { DOWNLOAD_STATUS } from '../constants'

const https = require("https");
const { pending, downloading, failed, completed } = DOWNLOAD_STATUS

@Processor('file')
export class DownloadConsumer {
  constructor(private downloadQueues: DownloadQueues, private downloadService: DownloadsService) { }

  @Process('transcode')
  async transcode(job: Job<unknown>) {
    try {
      const { file: { id, url, retries } }: any = job.data
      const filterBy = { _id: new ObjectId(id) }
      if (retries < 5) {
        const updatedStatus = await this.downloadService.findOneAndUpdate(filterBy, { status: downloading });

        https.get(url).on('response', (response) => {
          response.on('data', (data) => {
          });
          response.on('end', async () => {
            await this.downloadService.findOneAndUpdate(filterBy, { status: completed });
          });
          response.on('error', async () => {
            await this.downloadService.findOneAndUpdate(filterBy, { status: pending, });
            this.downloadQueues.addToQueue({ id, url, retries: retries + 1 })
          })
        }).on('error', async () => {
          await this.downloadService.findOneAndUpdate(filterBy, { status: pending, });
          this.downloadQueues.addToQueue({ id, url, retries: retries + 1 })
        })
        return updatedStatus;
      }
      await this.downloadService.findOneAndUpdate(filterBy, { status: failed, });
      return job
    }
    catch (err) {
      console.log(err, 'An error occured')
    }
  }
}
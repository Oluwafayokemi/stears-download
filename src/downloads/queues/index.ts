import { Injectable } from '@nestjs/common';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';

@Injectable()
export class DownloadQueues {
  constructor(@InjectQueue('file') private fileQueue: Queue){}

  async addToQueue(fileUrl: {}) {
    try {
     await this.fileQueue.add('transcode', {
        file: fileUrl,
      });
    }
    catch (err) {
      throw new Error(err)
    }
  }

}

export const DOWNLOAD_MODEL = "DOWNLOAD_MODEL"
export const DATABASE_CONNECTION = "DATABASE_CONNECTION"
export const DOWNLOAD_STATUS = {
  pending: 'pending',
  downloading: 'downloading',
  failed: 'failed',
  completed: 'completed'
}